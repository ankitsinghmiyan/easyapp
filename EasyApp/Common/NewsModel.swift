//
//  NewsModel.swift
//  EasyApp
//
//  Created by Tejas Inamdar on 23/06/18.
//  Copyright © 2018 Ankit. All rights reserved.
//

import Foundation
import UIKit

class NewsModel: NSObject {
    let newsAutherName:String?
    let newsTitle:String?
    let newsDesc:String?
    let newsUrl:String?
    let imageUrl:String?
    let newsDate:String?
    
    override init(){
        self.newsAutherName = ""
        self.newsTitle = ""
        self.newsDesc = ""
        self.newsUrl = ""
        self.imageUrl = ""
        self.newsDate = ""
    }
    
    init(autherName:String , newsTitle:String , newsDesc:String , newsUrl:String , imageUrl:String,newsDate:String ) {
        self.newsAutherName = autherName
        self.newsTitle = newsTitle
        self.newsDesc = newsDesc
        self.newsUrl = newsUrl
        self.imageUrl = imageUrl
        self.newsDate = newsDate
    }
}
