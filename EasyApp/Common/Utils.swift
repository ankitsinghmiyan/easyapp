//
//  Utils.swift
//  EasyApp
//
//  Created by Ankit on 23/06/18.
//  Copyright © 2018 Ankit. All rights reserved.
//

import Foundation
import UIKit
class Utils:NSObject {
    
    class func alertDialoagbox(title:String,message:String,controller:UIViewController) {
        let alertC = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let defaultAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
        alertC.addAction(defaultAction)
        controller.present(alertC, animated: true, completion: nil)
    }
    
}
class APIIntegration {
    static let appkey = "d30b3276092e4fc191a0ef8553fcfbd0"
    static let country = "in"
    static let url = "https://newsapi.org/v2/top-headlines?"
    static let appidKey = "apiKey="
    static let countryKey = "country="
    static let failed = "sorry"
    static let alert = "Alert"
    static let no_imagedata = "No image data is available for this image"
    static let no_weburl = "No details is available for this news"
    
    class func alertDialoagbox(title:String,message:String,controller:UIViewController) {
        let alertC = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let defaultAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
        alertC.addAction(defaultAction)
        controller.present(alertC, animated: true, completion: nil)
    }
}

import UIKit
extension UIStoryboard {
    
    //MARK:- Storyboards Instances
    
    class func mainStoryBoard() -> UIStoryboard {
        return UIStoryboard(name: "Main", bundle: nil)
    }
    
    class func dashboardStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "AuthStoryBoard", bundle: nil)
    }
    
    //MARK:- View Controllers in Main Storyboard.
    
        class func newsViewController() ->NewsViewController{
            return mainStoryBoard().instantiateViewController(withIdentifier: "NewsViewController") as! NewsViewController
        }
    
    class func cabViewController() ->CabViewController{
        return mainStoryBoard().instantiateViewController(withIdentifier: "CabViewController") as! CabViewController
    }
    
    class func booksViewController() ->BooksViewController{
        return mainStoryBoard().instantiateViewController(withIdentifier: "BooksViewController") as! BooksViewController
    }
    
    class func musicViewController() ->MusicViewController{
        return mainStoryBoard().instantiateViewController(withIdentifier: "MusicViewController") as! MusicViewController
    }
    
    //MARK:- View Controllers in Auth Storyboard.
    
    //    class func newsAppHomeViewController() ->NewsAppHomeViewController{
    //        return dashboardStoryboard().instantiateViewController(withIdentifier: "NewsAppHomeViewController") as! NewsAppHomeViewController
    //    }
    
}
extension UIView{
    func addShadowToButton(){
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 3, height: 3)
        self.layer.shadowRadius = 3
        self.layer.shadowOpacity = 1.0
        self.layer.cornerRadius = 10.0
        
    }
    
}
extension UIColor {
    static let backgroundColorForApp = UIColor(red: 170/255, green: 206/255, blue: 237/255, alpha: 1.0)
}
