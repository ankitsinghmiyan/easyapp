//
//  Constant.swift
//  EasyApp
//
//  Created by Tejas Inamdar on 23/06/18.
//  Copyright © 2018 Ankit. All rights reserved.
//

import Foundation
import UIKit
class Constant {
    static let appkey = "d30b3276092e4fc191a0ef8553fcfbd0"
    static let country = "in"
    static let url = "https://newsapi.org/v2/top-headlines?"
    static let appidKey = "apiKey="
    static let countryKey = "country="
    static let failed = "sorry"
    static let alert = "Alert"
    static let no_imagedata = "No image data is available for this image"
    static let no_weburl = "No details is available for this news"
    
    
    class func alertDialoagbox(title:String,message:String,controller:UIViewController) {
        let alertC = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let defaultAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
        alertC.addAction(defaultAction)
        controller.present(alertC, animated: true, completion: nil)
    }
}
