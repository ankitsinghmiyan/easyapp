//
//  StringsFile.swift
//  EasyApp
//
//  Created by Ankit on 23/06/18.
//  Copyright © 2018 Ankit. All rights reserved.
//

import Foundation
class StringFile:NSObject{
    static let splashScreen = "Welcome to Vision "
    static let dashboardScreen = "Welcome to Home. Here You can Read News , Books , Listen Music and Book Cabs."
    static let dashboardInstruction = "Say news, for news, say books, for books, say play, to Listen Music, say cab, to book cabs . "
    static let newSection = "Welcome, to news section"
    static let musicSection = "Welcome, to music section"
    static let bookSection = "Welcome, to book section"
    static let cabBookSection = "Welcome, to cabBook section"
    static let saysomething = "Please , Say something."
    static let tryAgain = "Please , Try again."
    static let news = "News"
    static let cab = "Cab"
    static let book = "Book"
    static let one  =  "One"
    static let music = "Play"
}
