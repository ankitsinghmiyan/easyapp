//
//  TextToSpeechUtilsClass.swift
//  EasyApp
//
//  Created by Ankit on 23/06/18.
//  Copyright © 2018 Ankit. All rights reserved.
//

import UIKit
import AVFoundation

class TextToSpeechUtilsClass:NSObject {
    
    class func textToSpeach(string:String)-> AVSpeechUtterance {
        let textinput = AVSpeechUtterance(string: string)
        textinput.voice = AVSpeechSynthesisVoice(language: "en-US")
        textinput.rate = 0.5
        return textinput
    }
}
