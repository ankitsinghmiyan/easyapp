//
//  TestViewController.swift
//  EasyApp
//
//  Created by Ankit on 23/06/18.
//  Copyright © 2018 Ankit. All rights reserved.
//

import UIKit
import AVFoundation
import Speech

class TestViewController: UIViewController , SFSpeechRecognizerDelegate{

    var sysnthesizer = AVSpeechSynthesizer()
    
    private let speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "en-US"))!//en-US
    private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    private var recognitionTask: SFSpeechRecognitionTask?
    private let audioEngine = AVAudioEngine()
    
    @IBOutlet weak var textField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        speechRecognizer.delegate = self
        
        voiceRecoAuth()

    }

    func voiceRecoAuth(){
        SFSpeechRecognizer.requestAuthorization { (authStatus) in
            let voice = TextToSpeechUtilsClass.textToSpeach(string: StringFile.saysomething)
            self.sysnthesizer.speak(voice)
            var isButtonEnabled = false
            
            switch authStatus {
            case .authorized:
                isButtonEnabled = true
                
            case .denied:
                isButtonEnabled = false
                print("User denied access to speech recognition")
                
            case .restricted:
                isButtonEnabled = false
                print("Speech recognition restricted on this device")
                
            case .notDetermined:
                isButtonEnabled = false
                print("Speech recognition not yet authorized")
            }
            
            OperationQueue.main.addOperation() {
                if self.audioEngine.isRunning {
                    self.audioEngine.stop()
                    self.recognitionRequest?.endAudio()
                } else {
                    self.startRecording()
                }
            }
        }
    }
    
    func startRecording() {
        
        if recognitionTask != nil {  //1
            recognitionTask?.cancel()
            recognitionTask = nil
        }
        
        let audioSession = AVAudioSession.sharedInstance()  //2
        do {
            try audioSession.setCategory(AVAudioSessionCategoryRecord)
            try audioSession.setMode(AVAudioSessionModeMeasurement)
            try audioSession.setActive(true, with: .notifyOthersOnDeactivation)
        } catch {
            print("audioSession properties weren't set because of an error.")
        }
        
        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        guard let inputNode = audioEngine.inputNode else {
            fatalError("Audio engine has no input node")
        }
        
        guard let recognitionRequest = recognitionRequest else {
            fatalError("Unable to create an SFSpeechAudioBufferRecognitionRequest object")
        } //5
        
        recognitionRequest.shouldReportPartialResults = true
        
        recognitionTask = speechRecognizer.recognitionTask(with: recognitionRequest, resultHandler: { (result, error) in
            
            var isFinal = false
            
            if result != nil {
                self.textField.text = result?.bestTranscription.formattedString
                //let diff = Tools.levenshtein(aStr: StringFile.news.uppercased(), bStr: (result?.bestTranscription.formattedString.uppercased())!)
                if StringFile.news.uppercased() == result?.bestTranscription.formattedString.uppercased() {
                    print("Ankit")
                } else {
                    self.voiceRecoAuth()
                }
//                if diff > 2  {
//                    let voice = TextToSpeechUtilsClass.textToSpeach(string: StringFile.saysomething)
//                    self.sysnthesizer.speak(voice)
//                    self.audioEngine.stop()
//                    self.recognitionRequest?.endAudio()
//                    self.voiceRecoAuth()
//                }else {
//                    print("Ankit")
//                }
                isFinal = (result?.isFinal)!
            }
            
            if error != nil || isFinal {  //10
                self.audioEngine.stop()
                inputNode.removeTap(onBus: 0)
                
                self.recognitionRequest = nil
                self.recognitionTask = nil
                
            }
        })
        
        let recordingFormat = inputNode.outputFormat(forBus: 0)  //11
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer, when) in
            self.recognitionRequest?.append(buffer)
        }
        
        audioEngine.prepare()  //12
        
        do {
            try audioEngine.start()
        } catch {
            print("audioEngine couldn't start because of an error.")
        }
        
        textField.text = "Say something, I'm listening!"
        
    }
    
    func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
        if available {
            
        } else {
            
        }
    }
}
