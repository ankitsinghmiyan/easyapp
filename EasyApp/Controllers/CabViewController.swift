//
//  CabViewController.swift
//  EasyApp
//
//  Created by Tejas Inamdar on 23/06/18.
//  Copyright © 2018 Ankit. All rights reserved.
//

import UIKit
import Speech

class CabViewController: UIViewController,SFSpeechRecognizerDelegate {

    var sysnthesizer = AVSpeechSynthesizer()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Cab Booking"
        let voice = TextToSpeechUtilsClass.textToSpeach(string: StringFile.cabBookSection)
        sysnthesizer.speak(voice)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
