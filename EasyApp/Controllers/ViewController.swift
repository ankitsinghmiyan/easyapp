//
//  ViewController.swift
//  EasyApp
//
//  Created by Ankit on 22/06/18.
//  Copyright © 2018 Ankit. All rights reserved.
//

import UIKit
import AVFoundation
import Speech

class ViewController: UIViewController {

    var sysnthesizer = AVSpeechSynthesizer()
    override func viewDidLoad() {
        super.viewDidLoad()
        let voice = TextToSpeechUtilsClass.textToSpeach(string: StringFile.splashScreen)
        sysnthesizer.speak(voice)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
 
    override func viewDidAppear(_ animated: Bool) {
        self.perform(#selector(nextScreen), with: nil, afterDelay: 4.0)
    }
    @objc func nextScreen() {
            self.performSegue(withIdentifier: "landingScreen", sender: self)
    }

}

