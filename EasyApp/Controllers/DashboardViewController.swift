//
//  DashboardViewController.swift
//  EasyApp
//
//  Created by Tejas Inamdar on 23/06/18.
//  Copyright © 2018 Ankit. All rights reserved.
//

import UIKit
import Foundation
import AVFoundation
import Speech

class DashboardViewController: UIViewController,SFSpeechRecognizerDelegate {

    @IBOutlet weak var newsBtn: UIButton!
    @IBOutlet weak var cabBtn: UIButton!
    @IBOutlet weak var booksBtn: UIButton!
    @IBOutlet weak var musicBtn: UIButton!
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var viewSctionone: UIView!
    @IBOutlet weak var viewSectionTwo: UIView!
    var sysnthesizer = AVSpeechSynthesizer()
    var flag = false
    //
    
    private let speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "en-US"))!//en-US
    private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    private var recognitionTask: SFSpeechRecognitionTask?
    private let audioEngine = AVAudioEngine()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "HOME"
        addShadowtoBtn()
        speechRecognizer.delegate = self
        viewSctionone.backgroundColor = UIColor.backgroundColorForApp
        viewSectionTwo.backgroundColor = UIColor.backgroundColorForApp
        playAppInstruction()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if flag == true {
            viewDidLoad()
        }
        flag = true
    }
   

    func playAppInstruction() {
        let voice = TextToSpeechUtilsClass.textToSpeach(string: StringFile.dashboardScreen)
        sysnthesizer.speak(voice)
        self.perform(#selector(usingInstruction), with: nil, afterDelay: 6.0)
    }
    
    @objc func usingInstruction() {
        let voice = TextToSpeechUtilsClass.textToSpeach(string: StringFile.dashboardInstruction)
        sysnthesizer.speak(voice)
        self.perform(#selector(saySomething), with: nil, afterDelay: 7.0)
    }
    
    func saySomething(){
//        let voice1 = TextToSpeechUtilsClass.textToSpeach(string: StringFile.saysomething)
//        sysnthesizer.speak(voice1)
         self.perform(#selector(voiceRecoAuth), with: nil, afterDelay: 2.0)
    }
    //MARK:- Reco Voice
 
    func voiceRecoAuth(){
        SFSpeechRecognizer.requestAuthorization { (authStatus) in
            var isButtonEnabled = false
            switch authStatus {
            case .authorized:
                isButtonEnabled = true
                
            case .denied:
                isButtonEnabled = false
                print("User denied access to speech recognition")
                
            case .restricted:
                isButtonEnabled = false
                print("Speech recognition restricted on this device")
                
            case .notDetermined:
                isButtonEnabled = false
                print("Speech recognition not yet authorized")
            }
            
            OperationQueue.main.addOperation() {
                if self.audioEngine.isRunning {
                    self.audioEngine.stop()
                    self.recognitionRequest?.endAudio()
                } else {
                    self.perform(#selector(self.startRecording), with: nil, afterDelay: 2.0)
                }
            }
        }
    }
    
    
    func startRecording() {
        if recognitionTask != nil {  //1
            recognitionTask?.cancel()
            recognitionTask = nil
        }
        
        let audioSession = AVAudioSession.sharedInstance()  //2
        do {
            try audioSession.setCategory(AVAudioSessionCategoryRecord)
            try audioSession.setMode(AVAudioSessionModeMeasurement)
            try audioSession.setActive(true, with: .notifyOthersOnDeactivation)
        } catch {
            print("audioSession properties weren't set because of an error.")
        }
        
        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()  //3
        guard let inputNode = audioEngine.inputNode else {
            fatalError("Audio engine has no input node")
        }
        
        guard let recognitionRequest = recognitionRequest else {
            fatalError("Unable to create an SFSpeechAudioBufferRecognitionRequest object")
        } //5
        
        recognitionRequest.shouldReportPartialResults = true  //6
        
        recognitionTask = speechRecognizer.recognitionTask(with: recognitionRequest, resultHandler: { (result, error) in  //7
            
            var isFinal = false  
            
            if result != nil {
                if StringFile.news.uppercased() == result?.bestTranscription.formattedString.uppercased() {
                    self.audioEngine.stop()
                    inputNode.removeTap(onBus: 0)
                    self.recognitionRequest = nil
                    self.recognitionTask = nil
                    if let topController = UIApplication.topViewController() {
                        if topController is NewsViewController {
                            return
                        }else {
                            self.launchNewsController()
                        }
                    }
                } else if StringFile.cab.uppercased() == result?.bestTranscription.formattedString.uppercased() {
                    self.audioEngine.stop()
                    inputNode.removeTap(onBus: 0)
                    self.recognitionRequest = nil
                    self.recognitionTask = nil
                    if let topController = UIApplication.topViewController() {
                        if topController is CabViewController {
                            return
                        }else {
                            self.launchCabController()
                        }
                    }
                } else if StringFile.book.uppercased() == result?.bestTranscription.formattedString.uppercased() {
                    self.audioEngine.stop()
                    inputNode.removeTap(onBus: 0)
                    self.recognitionRequest = nil
                    self.recognitionTask = nil
                    if let topController = UIApplication.topViewController() {
                        if topController is BooksViewController {
                            return
                        }else {
                            self.launchBooksController()
                        }
                    }
                } else if StringFile.music.uppercased() == result?.bestTranscription.formattedString.uppercased() {
                    self.audioEngine.stop()
                    inputNode.removeTap(onBus: 0)
                    self.recognitionRequest = nil
                    self.recognitionTask = nil
                    if let topController = UIApplication.topViewController() {
                        if topController is MusicViewController {
                            return
                        }else {
                            self.launchMusicController()
                        }
                    }
                } else {
                    //self.audioEngine.stop()
//                    inputNode.removeTap(onBus: 0)
//                    self.recognitionRequest = nil
//                    self.recognitionTask = nil
                    self.perform(#selector(self.voiceRecoAuth), with: nil, afterDelay: 2.0)
                }
                isFinal = (result?.isFinal)!
            }
            
//            else {
//                self.audioEngine.stop()
//                inputNode.removeTap(onBus: 0)
//                self.recognitionRequest = nil
//                self.recognitionTask = nil
//                //self.perform(#selector(self.startRecording), with: nil, afterDelay: 2.0)
//            }
            if error != nil || isFinal {  //10
                self.audioEngine.stop()
                inputNode.removeTap(onBus: 0)
                self.recognitionRequest = nil
                self.recognitionTask = nil
                self.perform(#selector(self.voiceRecoAuth), with: nil, afterDelay: 2.0)
            }
        })
        let recordingFormat = inputNode.outputFormat(forBus: 0)  //11
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer, when) in
            self.recognitionRequest?.append(buffer)
        }
        
        audioEngine.prepare()  //12
        do {
            try audioEngine.start()
        } catch {
            print("audioEngine couldn't start because of an error.")
        }
    }
    
    //Mark:Button Shadow
    func addShadowtoBtn(){
       newsBtn.addShadowToButton()
       cabBtn.addShadowToButton()
       booksBtn.addShadowToButton()
       musicBtn.addShadowToButton()
    }
    //Mark: Launch Controller Methods
    
    func launchNewsController(){
        let vc = UIStoryboard.newsViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func launchCabController(){
        let vc = UIStoryboard.cabViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func launchBooksController(){
        let vc = UIStoryboard.booksViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func launchMusicController(){
        let vc = UIStoryboard.musicViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func buttonOneClick(_ sender: UIButton) {
        self.audioEngine.stop()
        self.recognitionRequest = nil
        self.recognitionTask = nil
        self.audioEngine.inputNode?.removeTap(onBus: 0)
        launchNewsController()
        
    }
   
    @IBAction func buttonTwoClick(_ sender: UIButton) {
        self.audioEngine.stop()
        self.recognitionRequest = nil
        self.recognitionTask = nil
        self.audioEngine.inputNode?.removeTap(onBus: 0)
        launchCabController()
    }
    @IBAction func buttonThreeClick(_ sender: UIButton) {
        self.audioEngine.stop()
        self.recognitionRequest = nil
        self.recognitionTask = nil
        self.audioEngine.inputNode?.removeTap(onBus: 0)
        launchBooksController()
    }

    @IBAction func buttonFourClick(_ sender: UIButton) {
        self.audioEngine.stop()
        self.recognitionRequest = nil
        self.recognitionTask = nil
        self.audioEngine.inputNode?.removeTap(onBus: 0)
        launchMusicController()
    }
    
}

extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}
