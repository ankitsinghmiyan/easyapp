//
//  MusicViewController.swift
//  EasyApp
//
//  Created by Tejas Inamdar on 23/06/18.
//  Copyright © 2018 Ankit. All rights reserved.
//

import UIKit
import AVFoundation
import Speech

class MusicViewController: UIViewController,SFSpeechRecognizerDelegate {
    var songPlayer = AVAudioPlayer()
    var hasBeenPaused = false
    
    var sysnthesizer = AVSpeechSynthesizer()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Music"
        let voice = TextToSpeechUtilsClass.textToSpeach(string: StringFile.musicSection)
        sysnthesizer.speak(voice)
        self.perform(#selector(playMusic), with: nil, afterDelay: 3.0)
    }
    
    @objc func playMusic(){
        prepareSongAndSession()
        songPlayer.play()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func prepareSongAndSession() {
        
        do {
            songPlayer = try AVAudioPlayer(contentsOf: URL.init(fileURLWithPath: Bundle.main.path(forResource: "testAudio", ofType: "mp3")!))
            songPlayer.prepareToPlay()

            let audioSession = AVAudioSession.sharedInstance()
            do {
                try audioSession.setCategory(AVAudioSessionCategoryPlayback)
            } catch let sessionError {
                
                print(sessionError)
            }
        } catch let songPlayerError {
            print(songPlayerError)
        }
    }
    @IBAction func playBtn(_ sender: Any) {
        songPlayer.play()
    }
    @IBAction func pauseBtn(_ sender: Any) {
        if songPlayer.isPlaying {
            songPlayer.pause()
            hasBeenPaused = true
        } else {
            hasBeenPaused = false
        }
    }
    @IBAction func restartAction(_ sender: UIBarButtonItem) {
        if songPlayer.isPlaying || hasBeenPaused {
            songPlayer.stop()
            songPlayer.currentTime = 0
            
            songPlayer.play()
        } else  {
            songPlayer.play()
        }
    }
}

