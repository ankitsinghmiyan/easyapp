//
//  BooksViewController.swift
//  EasyApp
//
//  Created by Tejas Inamdar on 23/06/18.
//  Copyright © 2018 Ankit. All rights reserved.
//

import UIKit
import Speech


class BooksViewController: UIViewController,SFSpeechRecognizerDelegate {

    var sysnthesizer = AVSpeechSynthesizer()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Books"
        let voice = TextToSpeechUtilsClass.textToSpeach(string: StringFile.bookSection)
        sysnthesizer.speak(voice)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
