//
//  NewsViewController.swift
//  EasyApp
//
//  Created by Tejas Inamdar on 23/06/18.
//  Copyright © 2018 Ankit. All rights reserved.
//

import UIKit
import AVFoundation
import Alamofire
import KRProgressHUD
import AlamofireImage
import Speech

class NewsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,SFSpeechRecognizerDelegate{

    @IBOutlet weak var newsTableView: UITableView!
    var imageData:[Data] = []
    var newsModel:[NewsModel] = []
    var sysnthesizer = AVSpeechSynthesizer()
    
    
    private let speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "en-US"))!//en-US
    private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    private var recognitionTask: SFSpeechRecognitionTask?
    private let audioEngine = AVAudioEngine()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "News"
        speechRecognizer.delegate = self
        let voice = TextToSpeechUtilsClass.textToSpeach(string: StringFile.newSection)
        sysnthesizer.speak(voice)
        registerfirNib()
        requestapicall()
    }
    func registerfirNib(){
        let homeNib = UINib(nibName: "NewsTableViewCell", bundle: nil)
        newsTableView.register(homeNib, forCellReuseIdentifier: "NewsTableViewCell")
    }
 
    //Mark:Table View Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newsModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = newsTableView.dequeueReusableCell(withIdentifier: "NewsTableViewCell", for: indexPath) as! NewsTableViewCell
        
        cell.lbHeadingTitle.text = newsModel[indexPath.row].newsTitle
        cell.newsDescription.text = newsModel[indexPath.row].newsDesc
        
        let cache = UIImageView.af_sharedImageDownloader.imageCache
        if newsModel[indexPath.row].imageUrl == "" && newsModel[indexPath.row].imageUrl == nil {
            cell.newsImage.image = nil
            cache?.removeAllImages()
            cell.newsImage.image = UIImage(named: "ic_imageplaceholder")
        }else {
            cell.newsImage.image = nil
            cache?.removeAllImages()
            cell.newsImage.image = UIImage(data: imageData[indexPath.row])
    }
        return cell
}
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }
    func requestapicall(){
        KRProgressHUD.show(withMessage: "Please wait.....")
        let url = URL(string: "https://newsapi.org/v2/top-headlines?apikey=d30b3276092e4fc191a0ef8553fcfbd0&country=IN")
        Alamofire.request(url!).validate().responseJSON { (response) in
            if ((response.result.error) == nil) {
                let response =  response.result.value! as? [String:Any]
                let status = response!["status"] as! String
                if status == "ok" {
                    let object = response!["articles"] as! [Any]
                    for i in 0..<object.count {
                        let object_1 = object[i] as? [String:Any]
                        var newsauthor = object_1!["author"] as? String
                        if newsauthor == "" || newsauthor == nil {
                            newsauthor = ""
                        }
                        var newsdesc = object_1!["description"] as? String
                        if newsdesc == "" || newsdesc == nil {
                            newsdesc = ""
                        }
                        var newstitle = object_1!["title"] as? String
                        if newstitle == "" || newstitle == nil {
                            newstitle = ""
                        }
                        var newsdate = object_1!["publishedAt"] as? String
                        if newsdate == "" || newsdate == nil {
                            newsdate = ""
                        }
                        var newsimageurl = object_1!["urlToImage"] as? String
                        if newsimageurl == "" || newsimageurl == nil {
                            newsimageurl = ""
                        }
                        var weburl = object_1!["url"] as? String
                        if weburl == "" || weburl == nil {
                            weburl = ""
                        }
                        let dashboardNewsObj = NewsModel(autherName: newsauthor!, newsTitle: newstitle!, newsDesc: newsdesc!, newsUrl: weburl!, imageUrl: newsimageurl!, newsDate: newsdate!)
                        self.newsModel.append(dashboardNewsObj)
                    }
                    DispatchQueue.main.async {
                        for _ in 0..<self.newsModel.count {
                            let imgData = UIImagePNGRepresentation(UIImage(named: "ic_imageplaceholder")!)
                            self.imageData.append(imgData!)
                        }
                        self.newsTableView.reloadData()
                    }
                    let voice = TextToSpeechUtilsClass.textToSpeach(string: "You have received \(self.newsModel.count) news.")
                    self.sysnthesizer.speak(voice)
                    self.sayNews()
                    KRProgressHUD.dismiss()
                } else {
                    //try again
                    KRProgressHUD.dismiss()
                }
            } else {
                //alert dialog box
            }
        }
    }
    
    func sayNews() {
        var newsStr = ""
        for i in 0..<newsModel.count {
            newsStr = newsStr + "News \(i+1) is " + (newsModel[i].newsTitle ?? "")
        }
        let voice = TextToSpeechUtilsClass.textToSpeach(string: newsStr)
        self.sysnthesizer.speak(voice)
    }
    
    
}
